import gitlab
from project import copy_project
from variables_environnements import copy_variables_environnements_groupes



#variables
gl_source = gitlab.Gitlab(url='https://gitlab.com',private_token='glpat-eV386ot-zeMUg2M5GFbV')
gl_dest = gitlab.Gitlab(url='https://gitlab.allence.cloud',private_token='glpat-KeGyQsW-6P5-L67hKm-P')



def groupRecursively(group_id,parent_id):

    #Source Group
    Globalgroup = gl_source.groups.get(group_id)
    print(Globalgroup.name)
    #print("token of old tokens: ",Globalgroup.runners_token)



    #Create Group Destination
    if parent_id is None:
      groupo = gl_dest.groups.create({'name': Globalgroup.name, 'path': Globalgroup.path})
      copy_variables_environnements_groupes(Globalgroup.id,groupo.id)
      groups_tokens= gl_dest.groups.get(groupo.id)
      #print("token of new tokens: ",groups_tokens.runners_token)

    else:
        groupo = gl_dest.groups.create({'name': Globalgroup.name, 'path': Globalgroup.path, 'parent_id': parent_id})
        copy_variables_environnements_groupes(Globalgroup.id,groupo.id)
        groups_tokens= gl_dest.groups.get(groupo.id)
        #print("token of new tokens: ",groups_tokens.runners_token)
    #print(groupo.id)


    #Source SubGroups
    subGroups = gl_source.groups.get(group_id).subgroups.list(get_all=True)
    #print("subGroups",subGroups)

    #Source SubProjects
    projects = gl_source.groups.get(group_id).projects.list(get_all=True)
    #print("SubProjects",projects)


    #Create SubGroupe Project Destination
    for project in projects:
      if project.id==7701367 or project.id==15440120 or project.id==21149583 or project.id==18828733 or project.id==18828730 or project.id==17730866:
        print("skip chosa")
      else:
           #print("do")
      	   copy_project(project.id,groupo.id)
    #Create SubGroup
    for subGroup in subGroups:
        #print(subGroup.name)
        groupRecursively(subGroup.id,groupo.id)

